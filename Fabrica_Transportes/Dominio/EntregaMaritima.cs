﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    class EntregaMaritima : Logistica
    {
        public override ITransporte MetodoFabrica()
        {
            return new Barco();
        }
    }
}
