﻿using Fabrica_FactoryMethod.Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_Transportes.Dominio
{
    class Aviao : ITransporte
    {
        public string Entregar()
        {
            return "{Entrega de Avião}";
        }
    }
}
