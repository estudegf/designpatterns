﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    
    abstract class Logistica
    {
        
        public abstract ITransporte MetodoFabrica();

        public string Entregar()
        {
            // Call the factory method to create a Product object.
            var product = MetodoFabrica();
            // Now, use the product.
            var result = "Criador: o mesmo código criador funcionou para " + product.Entregar();

            return result;
        }
    }
}
