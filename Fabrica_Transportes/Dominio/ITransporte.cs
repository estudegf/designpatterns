﻿namespace Fabrica_FactoryMethod.Dominio
{
    // The Product interface declares the operations that all concrete products
    // must implement.
    public interface ITransporte
    {
        string Entregar();
    }
}