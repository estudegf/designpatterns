﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    class Barco : ITransporte
    {
        public string Entregar()
        {
            return "{Entrega de Barco}";
        }
    }
}
