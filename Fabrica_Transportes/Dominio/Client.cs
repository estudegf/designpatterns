﻿using Fabrica_Transportes.Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    class Client
    {
        public void Main()
        {
            Console.WriteLine("fazendo a entrega com a EntregaTerrestre.");
            RealizarEntrega(new EntregaTerrestre());

            Console.WriteLine("");

            Console.WriteLine("fazendo a entrega com a EntregaMaritima");
            RealizarEntrega(new EntregaMaritima());

            Console.WriteLine("");

            Console.WriteLine("fazendo a entrega com a EntregaAerea");
            RealizarEntrega(new EntregaAerea());
        }

        // The client code works with an instance of a concrete creator, albeit
        // through its base interface. As long as the client keeps working with
        // the creator via the base interface, you can pass it any creator's
        // subclass.
        public void RealizarEntrega(Logistica logistica)
        {
            // ...
            Console.WriteLine("Client: essa classe cliente não conhece a classe criadora," +
                "mas mesmo assim funciona.\n" + logistica.Entregar());
            // ...
        }
    }
}
