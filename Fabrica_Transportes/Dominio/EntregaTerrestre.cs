﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    class EntregaTerrestre : Logistica
    {
        public override ITransporte MetodoFabrica()
        {
            return new Caminhao();
        }
    }
}
