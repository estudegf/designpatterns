﻿using Fabrica_FactoryMethod.Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_Transportes.Dominio
{
    class EntregaAerea : Logistica
    {
        public override ITransporte MetodoFabrica()
        {
            return new Aviao();
        }
    }
}
