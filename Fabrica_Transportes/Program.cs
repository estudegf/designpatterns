﻿using Fabrica_FactoryMethod.Dominio;
using System;

namespace Fabrica_Transportes
{
    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
            Console.ReadLine();
        }
    }
}
