﻿using Fabrica_FactoryMethod.Dominio;
using System;

namespace Fabrica_FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
            Console.ReadLine();
        }
    }
}
