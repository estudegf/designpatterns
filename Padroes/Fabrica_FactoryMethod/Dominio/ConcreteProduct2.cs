﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrica_FactoryMethod.Dominio
{
    class ConcreteProduct2 : IProduct
    {
        public string Operation()
        {
            return "{Result of ConcreteProduct2}";
        }
    }
}
