﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Dominio
{
    public interface IBuilder
    {
        void BuildQueijo();

        void BuildCatupiry();

        void BuildFrango();
    }
}
