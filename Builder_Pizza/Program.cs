﻿using Builder;
using Builder.Dominio;
using System;

namespace Builder_Pizza
{
    class Program
    {
        static void Main(string[] args)
        {
            // The client code creates a builder object, passes it to the
            // director and then initiates the construction process. The end
            // result is retrieved from the builder object.
            var director = new Director();
            var builder = new PizzaBuilder();
            director.Builder = builder;

            Console.WriteLine("Produto Básico Padrão:");
            director.BuildMinimalViableProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            Console.WriteLine("Produto Completo Padrão:");
            director.BuildFullFeaturedProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            // Remember, the Builder pattern can be used without a Director
            // class.
            Console.WriteLine("Produto Customizado:");
            builder.BuildFrango();
            builder.BuildCatupiry();
            Console.Write(builder.GetProduct().ListParts());
            Console.ReadLine();
        }
    }
}
